create table mortgage_details (
dealNo int primary key auto_increment,
amount DOUBLE,
fname varchar(50),
lname varchar(50),
dob timestamp,
loanType char,
propertyAddress varchar(255),
monthlyIncome double,
monthlyExpenses double,
loanTerm double,
repaymentFrequency varchar(50),
status varchar(50),
submittedDate varchar(20)
)

create table profile_data(
profileid int primary key,
phoneno varchar(50),
address varchar(255),
dob timestamp,
pwd varchar(50)
)
