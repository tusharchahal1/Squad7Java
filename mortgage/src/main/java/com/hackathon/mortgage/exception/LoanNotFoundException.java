package com.hackathon.mortgage.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Loan Not Found")
public class LoanNotFoundException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LoanNotFoundException(int aLoanId )
	{
		super("Loan Id Not Found"+aLoanId);
	}

}
