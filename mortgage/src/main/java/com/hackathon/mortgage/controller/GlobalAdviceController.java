package com.hackathon.mortgage.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.hackathon.mortgage.model.ResponseMessage;



/**
 * @author Pranavi
 * ControllerAdvice class to generically handle exceptions in controller classes
 */
@ControllerAdvice
public class GlobalAdviceController {

	private static final Logger logger = LoggerFactory.getLogger(GlobalAdviceController.class);

	/**
	 * @param e
	 * @return
	 * method to handle exceptions from all controller classes and respond with the error response
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseMessage> handleException(Exception e) {
		logger.error(e.getMessage(), e);
		ResponseMessage msg = new ResponseMessage();
		msg.setMessage(e.getMessage());
		msg.setStatus(500);
		return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);
	}
}
