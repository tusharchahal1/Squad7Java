package com.hackathon.mortgage.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.mortgage.exception.LoanNotFoundException;
import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.service.RetreiveLoanIdService;

@RestController
@CrossOrigin
public class RetreiveMortgageController {

	
	private final Logger logger = LoggerFactory.getLogger(RetreiveMortgageController.class);
	 
	
	@Autowired
	RetreiveLoanIdService retLoanSer;
	
	
	@RequestMapping(value="/customer/{id}", method=RequestMethod.GET)
	public int getEmployee(@PathVariable("id") int id,HttpServletRequest  httpReq) throws LoanNotFoundException
	{
	
		Application app = retLoanSer.getLoanId(id);
		logger.debug(app.getFname());
		
		if(null==app){
			throw new LoanNotFoundException(id);
		}
		
	 return id;
	}
	
}
