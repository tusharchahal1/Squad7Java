package com.hackethon.mortgage.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hackathon.mortgage.dao.RetreiveLoanId;
import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.service.RetreiveLoanIdService;
import com.hackathon.mortgage.service.impl.RetreiveloanIdServiceImpl;

@Ignore
@RunWith(MockitoJUnitRunner.class)
@Configuration
public class RetreiveByLoanIdTest {
	
	
	@Configuration
	static class RetreiveLoanConfig
	{
		@Bean
		public RetreiveLoanIdService loanService()
		{
			return new RetreiveloanIdServiceImpl();
		}
		@Bean
		public RetreiveLoanId retLoanRepository()
		{
			return Mockito.mock(RetreiveLoanId.class);
		}
		
	}
	 
	@Autowired
	private RetreiveLoanIdService retLoanService;
	
	//@Autowired
	private RetreiveLoanId retreiveLoanId;
	
	Application testApp;
	
	@Before
	public void setup()
	{
		retreiveLoanId = Mockito.mock(RetreiveLoanId.class);
		retLoanService = new RetreiveloanIdServiceImpl();
		testApp = new Application();
		testApp.setAmount(1000);
		testApp.setFname("Vivek");
		testApp.setDealNo(1);
		
		Mockito.when(retreiveLoanId.retreiveLoanId(1)).thenReturn(testApp);
		
	}
	@After
	public void verify()
	{
		Mockito.verify(retreiveLoanId,VerificationModeFactory.times(1)).retreiveLoanId(Mockito.anyInt());
	}	
	
	
	@Test
	public void testRetreiveLoanById()
	{
		when(retreiveLoanId.retreiveLoanId(1)).thenReturn(testApp);	
		assertEquals(testApp.getDealNo(), retLoanService.getLoanId(1).getDealNo());
		
	}

}
